package invaders.game.render;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.event.Intercepts;
import invaders.framework.games.FrameTickEvent;

public final class AnimationSystem {
    private int alienAnimatedTicks = 0;

    public void run(final EntityDatabase db) {
        db.applyCombiner(this::animateSprite, SpriteComponent.class, VelocityComponent.class);
        db.applyCombiner(this::oscillateAliens, WobbleComponent.class, VelocityComponent.class);
    }

    private void animateSprite(SpriteComponent sprite, VelocityComponent velocity) {
        sprite.x += velocity.x;
        sprite.y += velocity.y;
    }

    private void oscillateAliens(WobbleComponent wobbler, VelocityComponent velocity) {
        if (this.alienAnimatedTicks % wobbler.getOscillationFrequency() == 0) {
            velocity.x *= -1;
        }
    }

    @Intercepts(FrameTickEvent.class)
    @SuppressWarnings("unused")
    public void onFrameTick(FrameTickEvent event) {
        ++this.alienAnimatedTicks;
    }
}
