package invaders.game.render;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public final class VelocityComponent implements Component, Prototype {

    float y;
    float x;

    public VelocityComponent(VelocityComponent target) {
        if (target != null) {
            this.x = target.x;
            this.y = target.y;
        }
    }

    public VelocityComponent(float x, float y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public Component reproduce() {
        return new VelocityComponent(this);
    }

    @Override
    public boolean equals(Component component) {
        if (!(component instanceof VelocityComponent)) {
            return false;
        }
        VelocityComponent velocity = (VelocityComponent) component;
        return velocity.y == y && velocity.x == x;
    }
}
