package invaders.game.render;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public final class WobbleComponent implements Component, Prototype {

    private float oscillationFrequency;

    private WobbleComponent(WobbleComponent target) {
        if (target != null) {
            this.oscillationFrequency = target.oscillationFrequency;
        }
    }
    
    public WobbleComponent(float oscillationFrequency) {
        this.oscillationFrequency = oscillationFrequency;
    }

    public float getOscillationFrequency() {
        return this.oscillationFrequency;
    }
    
    @Override
    public Component reproduce() {
        return new WobbleComponent(this);
    }
    
    @Override
    public boolean equals(Component component) {
        if (!(component instanceof WobbleComponent)) {
            return false;
        }
        WobbleComponent wobble = (WobbleComponent) component;
        return wobble.oscillationFrequency == oscillationFrequency;
    }
}
