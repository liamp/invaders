package invaders.game.render;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;
import invaders.game.GameResources;
import invaders.game.entity.CollisionComponent;

import java.awt.image.BufferedImage;

/**
 * @author Liam
 */
public final class SpriteComponent implements Component, Prototype {
    public final int w;
    public final int h;

    public int x;
    public int y;

    private final String resourceId;

    public SpriteComponent(final String resourceId, int x, int y) {
        this.resourceId = resourceId;
        this.x = x;
        this.y = y;
        this.w = GameResources.get(resourceId).getWidth(null);
        this.h = GameResources.get(resourceId).getHeight(null);
    }

    private SpriteComponent(final SpriteComponent target) {
        this.x = target.x;
        this.y = target.y;
        this.w = target.w;
        this.h = target.h;
        this.resourceId = target.resourceId;
    }

    public boolean collidesWith(final SpriteComponent other) {
        // FIXME Cheating here
        final CollisionComponent c0 = new CollisionComponent(this);
        final CollisionComponent c1 = new CollisionComponent(other);

        return c0.collides(c1);
    }

    BufferedImage getImg() {
        return GameResources.get(this.resourceId);
    }

    @Override
    public Component reproduce() {
        return new SpriteComponent(this);
    }

    @Override
    public boolean equals(Component component) {
        if (!(component instanceof SpriteComponent)) {
            return false;
        }
        SpriteComponent sprite = (SpriteComponent) component;
        return sprite.w == w && sprite.h == h && sprite.x == x && sprite.y == y;
    }
}
