package invaders.game.render;

import invaders.framework.ecs.ComponentConsumer;
import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.framework.event.Intercepts;
import invaders.game.ScoreEvent;
import invaders.game.entity.HealthComponent;

import java.awt.*;

public final class RenderSystem {
    private final ComponentConsumer<SpriteComponent> spriteRenderer;
    private Graphics graphicsContext;
    private int currentScore;

    @SuppressWarnings("FieldCanBeLocal")
    private boolean showHealthBars = false;

    public RenderSystem() {
        this.spriteRenderer = this::drawSprite;
        this.currentScore = 0;
    }

    public void run(final EntityDatabase entityDB) {
        if (this.showHealthBars) {
            entityDB.applyCombiner(this::drawHealthBar, SpriteComponent.class, HealthComponent.class);
        }
        entityDB.applyConsumer(this.spriteRenderer, SpriteComponent.class);
        this.graphicsContext.setColor(Color.WHITE);
        this.graphicsContext.drawString("Score: " + currentScore, 1200, 50);
    }

    private void drawSprite(SpriteComponent sprite, EntityId id) {
        this.graphicsContext.drawImage(sprite.getImg() ,sprite.x, sprite.y, null);
    }


    @Intercepts(ScoreEvent.class)
    public void updateScore(ScoreEvent scoreEvent){
        this.currentScore += scoreEvent.score;
    }

    private void drawHealthBar(SpriteComponent sprite, HealthComponent health) {
        this.graphicsContext.drawRect(sprite.x - 10, sprite.y + 10, (int) health.currentHealth,5);
    }

    public void setGraphicsContext(Graphics gr) {
        this.graphicsContext = gr;
    }
}
