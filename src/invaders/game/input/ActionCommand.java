
package invaders.game.input;

/**
 *
 * @author kyran
 */

@FunctionalInterface
public interface ActionCommand {
    void execute();
}




