
package invaders.game.input;

/**
 *
 * @author kyran
 */

import invaders.game.render.SpriteComponent;

public class CommandMoveLeft implements ActionCommand {

    private final SpriteComponent movableSprite;

    CommandMoveLeft(SpriteComponent movableSprite) {
        this.movableSprite = movableSprite;
    }

    @Override
    public void execute() {
        this.movableSprite.x -= 6;
    }
}
