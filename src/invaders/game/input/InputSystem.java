
package invaders.game.input;

/**
 *
 * @author kyran
 */

import invaders.framework.event.Intercepts;
import invaders.framework.input.KeyInputEvent;
import invaders.game.SpaceInvadersGame;
import invaders.game.render.SpriteComponent;

import java.awt.event.KeyEvent;
import java.util.HashMap;

public final class InputSystem {
    private final HashMap<Integer, ActionCommand> gameControls;

    private final SpaceInvadersGame game;
    private SpriteComponent movableSprite;

    public InputSystem(final SpaceInvadersGame game, final SpriteComponent movableSprite) {
        this.gameControls = new HashMap<>();
        this.game = game;
        this.movableSprite = movableSprite;

        this.initGameControlScheme();
    }

    private void initGameControlScheme() {
        this.gameControls.put(KeyEvent.VK_A, new CommandMoveLeft(this.movableSprite));
        this.gameControls.put(KeyEvent.VK_D, new CommandMoveRight(this.movableSprite));
        this.gameControls.put(KeyEvent.VK_W, new CommandFire(this.game));
        this.gameControls.put(KeyEvent.VK_L, new CommandSaveGame(this.game));
    }

    @Intercepts(KeyInputEvent.class)
    @SuppressWarnings("unused")
    public void keyPress(KeyInputEvent keyInput) {
        if (this.gameControls.containsKey(keyInput.keyCode)) {
            this.gameControls.get(keyInput.keyCode).execute();
        }
    }
}
