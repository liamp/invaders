
package invaders.game.input;

/**
 *
 * @author kyran
 */

import invaders.framework.ecs.EntityId;
import invaders.game.SpaceInvadersGame;

class CommandFire implements ActionCommand {
    private final SpaceInvadersGame game;

    CommandFire(final SpaceInvadersGame game) {
        this.game = game;
    }

    @Override
    public void execute() {
        final EntityId projectileId = this.game.projectileFactory.spawnProjectile(this.game.entityDB, this.game.playerSprite.x, this.game.playerSprite.y - 30);

        this.game.projectiles.add(projectileId);
    }
}
