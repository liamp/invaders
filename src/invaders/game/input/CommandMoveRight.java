
package invaders.game.input;

/**
 *
 * @author kyran
 */

import invaders.game.render.SpriteComponent;

class CommandMoveRight implements ActionCommand {

    private final SpriteComponent movableSprite;

    CommandMoveRight(SpriteComponent movableSprite) {
        this.movableSprite = movableSprite;
    }

    @Override
    public void execute() {
        this.movableSprite.x += 6;
    }
}