package invaders.game.input;

import invaders.game.SpaceInvadersGame;

import java.io.IOException;

class CommandSaveGame implements ActionCommand {
    private final SpaceInvadersGame gameInstance;

    CommandSaveGame(final SpaceInvadersGame game) {
        this.gameInstance = game;
    }

    @Override
    public void execute() {
        try {
            this.gameInstance.saveGame();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
