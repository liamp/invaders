package invaders.game;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.game.entity.CollisionComponent;
import invaders.game.entity.DamageComponent;
import invaders.game.render.SpriteComponent;
import invaders.game.render.VelocityComponent;

/**
 *
 * @author kyran
 */

public class ProjectileFactory {

    private final DamageComponent damage;
    private final VelocityComponent velocity;

    ProjectileFactory(VelocityComponent velocityPrototype, DamageComponent dmgPrototype) {
        this.damage = dmgPrototype;
        this.velocity = velocityPrototype;
    }

    public EntityId spawnProjectile(final EntityDatabase eDB, final int spawnX, final int spawnY) {
        final EntityId projectileId = new EntityId();
        final DamageComponent dmg = (DamageComponent) this.damage.reproduce();
        final VelocityComponent velocity = (VelocityComponent) this.velocity.reproduce();
        final SpriteComponent sprite = new SpriteComponent("player_ship", spawnX, spawnY);

        eDB.addComponentToEntity(projectileId, dmg);
        eDB.addComponentToEntity(projectileId, velocity);
        eDB.addComponentToEntity(projectileId, sprite);
        eDB.addComponentToEntity(projectileId, SpaceInvadersGame.PLAYER_TEAM);
        eDB.addComponentToEntity(projectileId, new CollisionComponent(sprite));

        return projectileId;
    }
}
