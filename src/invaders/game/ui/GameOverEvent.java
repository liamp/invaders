/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invaders.game.ui;

/**
 *
 * @author ericn
 */
public class GameOverEvent
{
    private final int score;
    private final boolean wasVictory;

    public GameOverEvent(boolean victory, int playerScore) {
        this.wasVictory = victory;
        this.score = playerScore;
    }

    public boolean wasVictory() {
        return this.wasVictory;
    }

    public int getScore() {
        return score;
    }
}
