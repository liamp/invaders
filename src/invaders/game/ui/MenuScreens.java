package invaders.game.ui;

import invaders.framework.event.Dispatcher;
import invaders.framework.games.GameApplication;
import invaders.framework.games.GameView;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;
import invaders.framework.user.GamesListView;
import invaders.framework.user.UserAccount;

import javax.swing.*;

/**
 *
 * @author kyran
 */

public class MenuScreens extends SwingView {

    public final static int SHOW_WIN = 1;
    public final static int SHOW_LOSE = 2;

    public MenuScreens(Dispatcher d, int screenSelect, GameApplication game, UserAccount user) {
        super(d, (screenSelect == SHOW_WIN) ? "Victory" : "Defeat");

        final JButton exitButton = new JButton("Exit");
        final JButton replayButton = new JButton("Replay");

        final JLabel label;
        if (screenSelect == SHOW_WIN) {
            label = new JLabel("You Won!");

            label.setBounds(80, 70, 200, 30);
            exitButton.setBounds(150, 160, 100, 30);
            replayButton.setBounds(250, 160, 100, 30);

            this.addComponent(label);
            this.addComponent(exitButton);
            this.addComponent(replayButton);
        } else if (screenSelect == SHOW_LOSE) {
            label = new JLabel("You Lost!");

            label.setBounds(80, 70, 200, 30);
            exitButton.setBounds(150, 160, 100, 30);
            replayButton.setBounds(250, 160, 100, 30);

            this.addComponent(label);
            this.addComponent(exitButton);
            this.addComponent(replayButton);
        }

        exitButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new GamesListView(d, user.getOwnedGames()))));
        replayButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new GameView(this.getDispatcher(), game))));
    }
}
    
