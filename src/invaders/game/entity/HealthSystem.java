package invaders.game.entity;

import invaders.framework.ecs.ComponentCombiner;
import invaders.game.render.SpriteComponent;

public class HealthSystem {
    private final ComponentCombiner<HealthComponent, DamageComponent> damageCalculator;

    public HealthSystem() { this.damageCalculator = this::calculateHealthAfterDamage; }

    private void calculateHealthAfterDamage(HealthComponent a, final DamageComponent b) {
        a.currentHealth -= b.damageValue;
    }

    private void checkCollisions(SpriteComponent projectileSprite, SpriteComponent collidee) {
        float minX0 = projectileSprite.x;
        float minX1 = collidee.x;
        float maxX0 = projectileSprite.x + projectileSprite.w;
        float maxX1 = collidee.x + collidee.w;
        float minY0 = projectileSprite.y;
        float minY1 = collidee.y;
        float maxY0 = projectileSprite.y + projectileSprite.h;
        float maxY1 = collidee.y + collidee.h;

        // Intersect
    }


    public ComponentCombiner<HealthComponent, DamageComponent> getDamageCalculator() {
        return this.damageCalculator;
    }
}
