package invaders.game.entity;

import invaders.framework.ecs.ComponentCombiner;
import invaders.framework.event.Dispatcher;

public class ScoreSystem {
    private Dispatcher dispatcher;
    private final ComponentCombiner<HealthComponent, ScoreComponent> scoreChecker;

    public ScoreSystem(Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
        this.scoreChecker = this::calculateScoreAfterKill;
    }

    private void calculateScoreAfterKill(HealthComponent a, ScoreComponent b) {
        if (a.currentHealth <= 0) {
//            dispatcher.dispatch(new ScoreEvent(b.score));
        }
    }

    public ComponentCombiner<HealthComponent, ScoreComponent> getScoreChecker() {
        return this.scoreChecker;
    }
}
