package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.framework.event.Dispatcher;
import invaders.game.ScoreEvent;
import invaders.game.render.SpriteComponent;

import java.util.ArrayList;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class CollisionSystem {

    private final EntityDatabase db;
    private final Consumer<EntityId> onCollision;
    private Dispatcher dispatcher;

    public CollisionSystem(final EntityDatabase db, final Consumer<EntityId> onCollision, Dispatcher dispatcher) {
        this.db = db;
        this.onCollision = onCollision;
        this.dispatcher = dispatcher;
    }

    private Stream<EntityId> getColliders(final EntityId e) {
        final SpriteComponent c = (SpriteComponent) this.db.getComponentTable(SpriteComponent.class).get().get(e);

        if (c != null) {
            final Predicate<SpriteComponent> checker = (otherComponent) -> otherComponent.collidesWith(c);
            return this.db.findAll(checker, SpriteComponent.class);
        } else {
            return Stream.empty();
        }
    }

    private void calculateScoreAfterKill(HealthComponent a, ScoreComponent b, float dt) {
        dispatcher.dispatch(new ScoreEvent(b.score));
    }

    public void checkCollision(final EntityId e) {
        // All of the entities which have sprite components colliding with C
        final DamageComponent du = (DamageComponent) this.db.getComponentTable(DamageComponent.class).get().get(e);
        final TeamComponent projectileTeam = (TeamComponent) this.db.getComponentTable(TeamComponent.class).get().get(e);

        if (du != null) {
            final Map<EntityId, Component> healthComponents = this.db.getComponentTable(HealthComponent.class).get();
            final Map<EntityId, Component> teamComponents = this.db.getComponentTable(TeamComponent.class).get();
            final Map<EntityId, Component> scoreComponents = this.db.getComponentTable(ScoreComponent.class).get();

            final ArrayList<EntityId> colliders = (ArrayList<EntityId>) this.getColliders(e).collect(Collectors.toList());

            for (EntityId entityId : colliders) {
                final HealthComponent entityHealth = (HealthComponent) healthComponents.get(entityId);
                final TeamComponent entityTeam = (TeamComponent) teamComponents.get(entityId);


                final ScoreComponent entityScore = (ScoreComponent) scoreComponents.get(entityId);
                if (entityHealth != null && projectileTeam.team != entityTeam.team) {

                    entityHealth.currentHealth = entityHealth.currentHealth - du.damageValue;
                    if (entityHealth.currentHealth <= 0 && entityScore != null) {
                        calculateScoreAfterKill(entityHealth, entityScore, 0);
                        this.onCollision.accept(entityId);
                    }

                    this.db.deleteEntity(e);

                    return;
                }
            }
        }
    }

}
