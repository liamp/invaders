/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invaders.game.entity;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.framework.games.ResourceCatalog;
import invaders.game.SpaceInvadersGame;
import invaders.game.render.SpriteComponent;
import invaders.game.render.VelocityComponent;
import invaders.game.render.WobbleComponent;

/**
 * @author ericn
 */
public class TankAlienFactory extends AlienFactory {
    public TankAlienFactory(ResourceCatalog gameResources, final int alienOffsetX) {
        super(gameResources, alienOffsetX);
    }


    @Override
    public void buildAlien(EntityDatabase eDB, int alienX, int alienY) {
        EntityId eId = new EntityId();
        HealthComponent alienHealth = new HealthComponent(200.00f);
        ScoreComponent alienScore = new ScoreComponent(100);
        VelocityComponent alienVelocity = new VelocityComponent(1.0f,0.0f);

        SpriteComponent alienSprite = new SpriteComponent("tank_alien", this.alienOffsetX + alienX * 100, alienY * 50);
        WobbleComponent alienWobble = new WobbleComponent(100.0f);
        eDB.addComponentToEntity(eId, alienSprite);
        eDB.addComponentToEntity(eId, alienHealth);
        eDB.addComponentToEntity(eId, alienHealth);
        eDB.addComponentToEntity(eId, alienScore);
        eDB.addComponentToEntity(eId, alienVelocity);
        eDB.addComponentToEntity(eId, alienWobble);
        eDB.addComponentToEntity(eId, SpaceInvadersGame.ENEMY_TEAM);
    }
}
