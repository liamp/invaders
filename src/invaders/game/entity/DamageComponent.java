package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public class DamageComponent implements Component, Prototype {

    public float damageValue;

    DamageComponent(DamageComponent target) {
        if (target != null) {
            this.damageValue = target.damageValue;
        }
    }

    public DamageComponent(float v) {
        this.damageValue = v;
    }

    @Override
    public Component reproduce() {
        return new DamageComponent(this);
    }

    @Override
    public boolean equals(Component component) {
        if (!(component instanceof DamageComponent)) {
            return false;
        }
        DamageComponent damage = (DamageComponent) component;
        return damage.damageValue == damageValue;
    }
}
