/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invaders.game.entity;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.games.ResourceCatalog;

/**
 * @author ericn
 */
abstract class AlienFactory {
    final ResourceCatalog gameResources;

    protected final int alienOffsetX;

    AlienFactory(final ResourceCatalog gameResources, int alienOffsetX) {
        this.gameResources = gameResources;
        this.alienOffsetX = alienOffsetX;
    }

    AlienFactory(final ResourceCatalog gameResources) {
        this.gameResources = gameResources;
        this.alienOffsetX = 0;
    }

    abstract void buildAlien(EntityDatabase eDB, int aX, int aY);
}
