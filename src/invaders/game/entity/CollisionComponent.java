package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;
import invaders.game.render.SpriteComponent;

public final class CollisionComponent implements Component, Prototype {

    private int minX;
    private int maxX;
    private int minY;
    private int maxY;

    private CollisionComponent(CollisionComponent target) {
        if (target != null) {
            this.minX = target.minX;
            this.maxX = target.maxX;
            this.minY = target.minY;
            this.maxY = target.maxY;

        }
    }

    public CollisionComponent(final SpriteComponent spriteBounds) {
        this.minX = spriteBounds.x;
        this.maxX = spriteBounds.x + spriteBounds.w;
        this.minY = spriteBounds.y;
        this.maxY = spriteBounds.y + spriteBounds.h;
    }

    public boolean collides(CollisionComponent other) {
        return (this.minX <= other.maxX && this.maxX >= other.minX && this.minY <= other.minY && this.maxY >= other.minY);
    }

    @Override
    public Component reproduce() {
        return new CollisionComponent(this);
    }

    @Override
    public boolean equals(Component component) {
        if (!(component instanceof CollisionComponent)) {
            return false;
        }
        CollisionComponent collision = (CollisionComponent) component;
        return collision.minX == minX && collision.maxX == maxX && collision.minY == minY && collision.maxY == maxY;
    }
}
