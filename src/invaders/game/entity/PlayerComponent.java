package invaders.game.entity;

import invaders.framework.ecs.Component;

public class PlayerComponent implements Component {
    public final boolean isPlayer = true;
}
