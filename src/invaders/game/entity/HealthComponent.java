package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public class HealthComponent implements Component, Prototype {

    public float currentHealth;
    private float maxHealth;

    private HealthComponent(HealthComponent target) {
        if (target != null) {
            this.currentHealth = target.currentHealth;
            this.maxHealth = target.maxHealth;
        }
    }

    public HealthComponent(float maxHealth) {
        this.currentHealth = maxHealth;
        this.maxHealth = maxHealth;
    }

    @Override
    public Component reproduce() {
        return new HealthComponent(this);
    }

    @Override
    public boolean equals(Component component) {
        if (!(component instanceof HealthComponent)) {
            return false;
        }
        HealthComponent health = (HealthComponent) component;
        return health.currentHealth == currentHealth && health.maxHealth == maxHealth;
    }
}
