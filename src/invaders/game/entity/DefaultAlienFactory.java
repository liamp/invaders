/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invaders.game.entity;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.framework.games.ResourceCatalog;
import invaders.game.SpaceInvadersGame;
import invaders.game.render.SpriteComponent;
import invaders.game.render.VelocityComponent;
import invaders.game.render.WobbleComponent;

import java.awt.image.BufferedImage;

/**
 * @author ericn
 */
public class DefaultAlienFactory extends AlienFactory {
    public DefaultAlienFactory(ResourceCatalog gameResources, int alienOffsetX) {
        super(gameResources, alienOffsetX);
    }

    @Override
    public void buildAlien(EntityDatabase eDB, int alienX, int alienY) {
        EntityId eId = new EntityId();

        SpriteComponent alienSprite = new SpriteComponent("default_alien", this.alienOffsetX + alienX * 100, alienY * 50);
        HealthComponent alienHealth = new HealthComponent(100.00f);
        ScoreComponent alienScore = new ScoreComponent(100);
        VelocityComponent alienVelocity = new VelocityComponent(1.0f, 0.0f);
        WobbleComponent alienWobble = new WobbleComponent(100.00f);

        eDB.addComponentToEntity(eId, alienSprite);
        eDB.addComponentToEntity(eId, alienHealth);
        eDB.addComponentToEntity(eId, alienScore);
        eDB.addComponentToEntity(eId, alienVelocity);
        eDB.addComponentToEntity(eId, alienWobble);
        eDB.addComponentToEntity(eId, SpaceInvadersGame.ENEMY_TEAM);
    }
}
