package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public class ScoreComponent implements Component, Prototype{
    public int score;
    
    private ScoreComponent(ScoreComponent target) {
        if (target != null) {
            this.score = target.score;
        }
    }

    ScoreComponent(int score){
        this.score = score;
    }
    
    @Override
    public Component reproduce() {
        return new ScoreComponent(this);
    }
    
    @Override
    public boolean equals(Component component) {
        if (!(component instanceof ScoreComponent)) {
            return false;
        }
        ScoreComponent scoreComp = (ScoreComponent) component;
        return scoreComp.score == score;
    }
}
