package invaders.game.entity;

import invaders.framework.ecs.Component;
import invaders.framework.ecs.Prototype;

public final class TeamComponent implements Component, Prototype {
    public int team;
    
     private TeamComponent(TeamComponent target) {
        if (target != null) {
            this.team = target.team;
        }
    }

    public TeamComponent(final int team) {
        this.team = team;
    }

    @Override
    public Component reproduce() {
        return new TeamComponent(this);
    }
    
    @Override
    public boolean equals(Component component) {
        if (!(component instanceof TeamComponent)) {
            return false;
        }
        TeamComponent teamComp = (TeamComponent) component;
        return teamComp.team == team;
    }
}
