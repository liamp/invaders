package invaders.game;

public class ScoreEvent {
    public final int score;

    public ScoreEvent(final int score){
        this.score = score;
    }
}
