package invaders.game;

import invaders.framework.data.JsonRepository;
import invaders.framework.data.ModelAdapter;
import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.framework.event.Dispatcher;
import invaders.framework.event.Intercepts;
import invaders.framework.games.GameApplication;
import invaders.framework.swingui.Redirect;
import invaders.framework.user.UserAccount;
import invaders.game.entity.*;
import invaders.game.input.InputSystem;
import invaders.game.render.AnimationSystem;
import invaders.game.render.RenderSystem;
import invaders.game.render.SpriteComponent;
import invaders.game.render.VelocityComponent;
import invaders.game.ui.GameOverEvent;
import invaders.game.ui.GameQuitEvent;
import invaders.game.ui.MenuScreens;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class SpaceInvadersGame extends GameApplication {
    public static final TeamComponent PLAYER_TEAM = new TeamComponent(0);
    public static final TeamComponent ENEMY_TEAM = new TeamComponent(1);

    private static final int ALIEN_OFFSET_X = 640 - (240);

    private final RenderSystem renderSystem;
    private final HealthSystem healthSystem;
    private InputSystem inputSystem;
    private final CollisionSystem collisionSystem;
    private final ScoreSystem scoreSystem;
    private final AnimationSystem animationSystem;
    public final ProjectileFactory projectileFactory;

    private final DefaultAlienFactory defaultAlienFactory;
    private final FastAlienFactory fastAlienFactory;
    private final TankAlienFactory tankAlienFactory;
    private final Dispatcher dispatcher;

    private HealthComponent playerHealth;
    public SpriteComponent playerSprite;

    public final ArrayList<EntityId> projectiles = new ArrayList<>();

    private int playerScore;
    private int alienCount;

    public SpaceInvadersGame(final Dispatcher dispatcher, final UserAccount user) throws IOException {
        this(dispatcher, new EntityDatabase(), user);
    }

    // If new game -> then initialise player + aliens
    // otherwise -> load game from database, already have player initialised.
    public SpaceInvadersGame(final Dispatcher dispatcher, final EntityDatabase db, final UserAccount user) throws IOException {
        super(dispatcher, db, new JsonRepository<>(ModelAdapter.getWrappedClass(EntityDatabase.Snapshot.class), "newsave.json", new EntityDatabase.InterfaceAdapter()), user);

        GameResources.loadResources();

        this.initPlayer();
        this.dispatcher = dispatcher;
        this.renderSystem = new RenderSystem();
        this.healthSystem = new HealthSystem();
        this.scoreSystem = new ScoreSystem(dispatcher);
        this.animationSystem = new AnimationSystem();
        this.defaultAlienFactory = new DefaultAlienFactory(GameResources.getResourceCatalog(), ALIEN_OFFSET_X);
        this.tankAlienFactory = new TankAlienFactory(GameResources.getResourceCatalog(), ALIEN_OFFSET_X);
        this.fastAlienFactory = new FastAlienFactory(GameResources.getResourceCatalog(), ALIEN_OFFSET_X);
        this.projectileFactory = new ProjectileFactory(new VelocityComponent(0.0f, -10.0f), new DamageComponent(100.0f));

        this.collisionSystem = new CollisionSystem(this.entityDB, this::onEnemyProjectileCollision, this.dispatcher);

        dispatcher.register(this.animationSystem);
        dispatcher.register(this.collisionSystem);
        dispatcher.register(this.renderSystem);
        dispatcher.register(this);
        this.buildAliens();

        this.inputSystem = new InputSystem(this, this.playerSprite);
        this.dispatcher.register(this.inputSystem);
    }

    @Override
    public void loadGame(final EntityDatabase.Snapshot db) {
        this.entityDB.restore(db);
        final EntityId playerId = (EntityId) this.entityDB.getComponentTable(PlayerComponent.class).get().keySet().toArray()[0];
        this.playerSprite = (SpriteComponent) this.entityDB.getComponentTable(SpriteComponent.class).get().get(playerId);
        this.entityDB.getComponentTable(SpriteComponent.class).get().put(playerId, this.playerSprite);
        this.dispatcher.unregister(this.inputSystem);
        this.inputSystem = new InputSystem(this, playerSprite);
        this.dispatcher.register(this.inputSystem);
    }

    private void onEnemyProjectileCollision(EntityId entityId) {
        --this.alienCount;
    }


    private boolean deadEntityFilter(final HealthComponent health) {
        return health.currentHealth <= 0;
    }

    private void buildAliens() {
        for (int alienX = 0; alienX < 5; ++alienX) {
            for (int alienY = 0; alienY < 6; ++alienY) {
                if (alienY == 0 || alienY == 5) {
                    tankAlienFactory.buildAlien(this.entityDB, alienX, alienY);
                } else if (alienY == 1 || alienY == 4) {
                    fastAlienFactory.buildAlien(this.entityDB, alienX, alienY);
                } else {
                    defaultAlienFactory.buildAlien(this.entityDB, alienX, alienY);
                }
            }
        }

        this.alienCount = 30;
    }

    private void initPlayer() {
        final EntityId pId = new EntityId();
        this.playerSprite = new SpriteComponent("player_ship", 640, 600);
        this.playerHealth = new HealthComponent(150.0f);
        this.entityDB.addComponentToEntity(pId, this.playerSprite);
        this.entityDB.addComponentToEntity(pId, this.playerHealth);
        this.entityDB.addComponentToEntity(pId, PLAYER_TEAM);
        this.entityDB.addComponentToEntity(pId, new PlayerComponent());
    }

    @Override
    public void updateAndRender(Graphics gr) {
        gr.setColor(Color.BLACK);
        gr.drawRect(0, 0, 1280, 720);

        gr.setColor(Color.RED);
        // If the player's health reaches zero, signal the game has ended.
        if (this.playerHealth.currentHealth <= 0) {
            this.dispatcher.dispatch(new GameOverEvent(false, this.playerScore));
        } else if (this.alienCount <= 0) {
            this.dispatcher.dispatch(new GameOverEvent(true, this.playerScore));
        } else {
            this.entityDB.applyCombiner(this.healthSystem.getDamageCalculator(), HealthComponent.class, DamageComponent.class);
            this.animationSystem.run(this.entityDB);
            this.entityDB.applyCombiner(this.scoreSystem.getScoreChecker(), HealthComponent.class, ScoreComponent.class);
            this.projectiles.forEach(this.collisionSystem::checkCollision);

            // Render last
            this.renderSystem.setGraphicsContext(gr);
            this.renderSystem.run(this.entityDB);
            this.entityDB.deleteAll(this.entityDB.findAll(this::deadEntityFilter, HealthComponent.class).collect(Collectors.toSet()));
        }
    }

    @Override
    public String getGameTitle() {
        return "Space Invaders";
    }

    @Intercepts(GameQuitEvent.class)
    @SuppressWarnings("unused")
    public void saveProgress(GameQuitEvent event) {
        try {
            this.saveGame();
            this.dispatcher.dispatch(new Redirect.Back());
        } catch (IOException ex) {
            Logger.getLogger(SpaceInvadersGame.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Intercepts(GameOverEvent.class)
    @SuppressWarnings("unused")
    public void gameOver(GameOverEvent event) {
        if (event.wasVictory()) {
            this.dispatcher.dispatch(new Redirect(new MenuScreens(this.dispatcher, MenuScreens.SHOW_WIN, this, this.playingUser)));
        } else {
            this.dispatcher.dispatch(new Redirect(new MenuScreens(this.dispatcher, MenuScreens.SHOW_LOSE, this, this.playingUser)));
        }
    }
}
