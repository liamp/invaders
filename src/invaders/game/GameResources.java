package invaders.game;

import invaders.framework.games.ResourceCatalog;

import java.awt.image.BufferedImage;
import java.io.IOException;

public class GameResources {
    private static final ResourceCatalog resourceCatalog = new ResourceCatalog();

    static void loadResources() throws IOException {
        resourceCatalog.loadTexture("default_alien", "resources/defaultAlien.png");
        resourceCatalog.loadTexture("player_ship", "resources/Player.png");
        resourceCatalog.loadTexture("tank_alien", "resources/tankAlien.png");
        resourceCatalog.loadTexture("fast_alien", "resources/fastAlien.png");
    }

    static ResourceCatalog getResourceCatalog() {
        return resourceCatalog;
    }

    public static BufferedImage get(String spriteId) {
        return resourceCatalog.getTexture(spriteId);
    }
}
