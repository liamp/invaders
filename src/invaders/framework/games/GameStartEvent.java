package invaders.framework.games;

public final class GameStartEvent {
    public final GameApplication game;

    public GameStartEvent(GameApplication game) {
        this.game = game;
    }
}
