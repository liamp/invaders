package invaders.framework.games;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ResourceCatalog {
    private final Map<String, BufferedImage> textures;

    public ResourceCatalog() {
        this.textures = new HashMap<>();
    }

    public void loadTexture(String resourceName, String path) throws IOException {
        final File resourceFile = new File(path);

        this.textures.put(resourceName, ImageIO.read(resourceFile));
    }

    public void deleteTexture(String resourceName) {
        this.textures.remove(resourceName);
    }

    public BufferedImage getTexture(String resourceName) {
        return this.textures.get(resourceName);
    }
}
