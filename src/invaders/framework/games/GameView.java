package invaders.framework.games;

import invaders.framework.event.Dispatcher;
import invaders.framework.swingui.SwingView;

import java.awt.*;

public class GameView extends SwingView {

    public GameView(Dispatcher d, final GameApplication gameApp) {
        super(d, gameApp.getGameTitle());
        this.getContent().setLayout(new FlowLayout());
        final GameCanvas canvas = new GameCanvas(gameApp, d);
        this.getContent().setPreferredSize(new Dimension(1280, 720));
        canvas.setPreferredSize(new Dimension(1280, 720));
        this.addComponent(canvas);
        this.getContent().setVisible(true);
        this.getContent().requestFocusInWindow();
    }

}
