package invaders.framework.games;

import invaders.framework.data.ModelAdapter;
import invaders.framework.data.Repository;
import invaders.framework.ecs.EntityDatabase;
import invaders.framework.event.Dispatcher;
import invaders.framework.user.UserAccount;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.stream.Collectors;

public abstract class GameApplication {

    protected final Dispatcher dispatcher;
    public final EntityDatabase entityDB;
    private final Repository<ModelAdapter<EntityDatabase.Snapshot>> savedGames;

    protected final UserAccount playingUser;

    public GameApplication(final Dispatcher dispatcher, final EntityDatabase entityDB, final Repository<ModelAdapter<EntityDatabase.Snapshot>> savedGames, final UserAccount user) {
        this.dispatcher = dispatcher;
        this.entityDB = entityDB;
        this.savedGames = savedGames;
        this.playingUser = user;
    }

    public void loadGame(final EntityDatabase.Snapshot db) {
        this.entityDB.restore(db);
    }

    public void saveGame() throws IOException {
        final ModelAdapter<EntityDatabase.Snapshot> savedState = ModelAdapter.of(this.entityDB.saveState());
        this.savedGames.insert(savedState);
    }

    public abstract void updateAndRender(Graphics gr);

    public abstract String getGameTitle();
    
    public ArrayList<EntityDatabase.Snapshot> getSaves() {
        return (ArrayList<EntityDatabase.Snapshot>) this.savedGames.all().stream().map(ModelAdapter::get).collect(Collectors.toList());
    }
}
