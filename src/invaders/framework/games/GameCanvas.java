package invaders.framework.games;

import invaders.framework.event.Dispatcher;
import invaders.framework.event.Intercepts;
import invaders.framework.input.KeyInputEvent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

class GameCanvas extends JPanel {
    private static final int TIMER_DELAY_MILLISECONDS = 33;

    private final GameApplication game;
    private final Timer gameUpdateTimer;
    private final Dispatcher dispatcher;
    private final FrameTickEvent frameTickEventFlyweight = new FrameTickEvent();

    GameCanvas(final GameApplication game, final Dispatcher dispatcher) {
        this.game = game;
        this.gameUpdateTimer = new Timer(TIMER_DELAY_MILLISECONDS, e -> this.repaint());
        this.gameUpdateTimer.setRepeats(true);
        this.gameUpdateTimer.start();
        this.dispatcher = dispatcher;
        this.dispatcher.register(this);
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);
    }

    @Intercepts(KeyInputEvent.class)
    public void onKey(KeyInputEvent k) {
        if (k.keyCode == KeyEvent.VK_B) {
            if (this.gameUpdateTimer.isRunning()) {
                this.gameUpdateTimer.stop();
            } else {
                this.gameUpdateTimer.start();
            }
        }
    }
    @Override
    public void paint(Graphics gr) {
        super.paint(gr);

        gr.setColor(Color.GREEN);

        this.dispatcher.dispatch(this.frameTickEventFlyweight);
        this.game.updateAndRender(gr);
    }

    //@Intercepts(GameOverEvent.class)
    public void pause() {
        
        this.gameUpdateTimer.stop();
    }

    public void start() {
        this.gameUpdateTimer.restart();
    }
}
