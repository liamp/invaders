package invaders.framework.games;

public class FrameTickEvent {
    private float dt;

    public float getDeltaTime() {
        return this.dt;
    }

    public void setDeltaTime(float newDt) {
        this.dt = newDt;
    }
}
