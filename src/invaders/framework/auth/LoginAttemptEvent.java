package invaders.framework.auth;

public final class LoginAttemptEvent {
    final String username;
    final String password;

    public LoginAttemptEvent(final String username, final String password) {
        this.username = username;
        this.password = password;
    }
}
