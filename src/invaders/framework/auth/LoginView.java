package invaders.framework.auth;

import invaders.framework.event.Dispatcher;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;

import javax.swing.*;

public class LoginView extends SwingView {

    private final JLabel usernameLabel;
    private final JLabel passwordLabel;
    private final JTextField usernameInput;
    private final JPasswordField passwordInput;
    private final JButton loginButton;
    private final JButton registerButton;

    public LoginView(Dispatcher d) {
        super(d, "Login");

        usernameLabel = new JLabel("Username");
        passwordLabel = new JLabel("Password");
        usernameInput = new JTextField();
        passwordInput = new JPasswordField();
        loginButton = new JButton("Login");
        registerButton = new JButton("Register");

        usernameLabel.setBounds(80, 70, 200, 30);
        passwordLabel.setBounds(80, 110, 200, 30);
        usernameInput.setBounds(300, 70, 200, 30);
        passwordInput.setBounds(300, 110, 200, 30);
        loginButton.setBounds(150, 160, 100, 30);
        registerButton.setBounds(250, 160, 100, 30);


        this.addComponent(usernameLabel);
        this.addComponent(passwordLabel);
        this.addComponent(usernameInput);
        this.addComponent(passwordInput);
        this.addComponent(loginButton);
        this.addComponent(registerButton);

        this.loginButton.addActionListener(e -> getDispatcher().dispatch(new LoginAttemptEvent(getUsernameInput(), getPasswordInput())));
        this.registerButton.addActionListener(e -> getDispatcher().dispatch(new Redirect(new RegisterView(getDispatcher()))));
    }

    public String getUsernameInput() {
        return this.usernameInput.getText();
    }

    public String getPasswordInput() {
        return new String(this.passwordInput.getPassword());
    }
}
