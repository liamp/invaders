package invaders.framework.auth;

import invaders.framework.Controller;
import invaders.framework.Framework;
import invaders.framework.data.Repository;
import invaders.framework.event.Intercepts;
import invaders.framework.swingui.PopupMessageEvent;
import invaders.framework.user.DashboardView;
import invaders.framework.user.UserAccount;

import java.io.IOException;
import java.util.Optional;

public class AuthController extends Controller {

    private final Repository<UserAccount> users;

    public AuthController(Framework app, Repository<UserAccount> users) {
        super(app);
        this.users = users;
    }

    @Intercepts(LoginAttemptEvent.class)
    @SuppressWarnings("unused")
    public void attemptLogin(LoginAttemptEvent login) {
        Optional<UserAccount> maybeUser = this.users.findFirst(u -> u.validateCredentials(login.username, login.password));
        if (maybeUser.isPresent()) {
            final UserAccount user = maybeUser.get();
            try {
                this.app.setLoggedInUser(user);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.dispatchRedirect(new DashboardView(app.getDispatcher(), user));
        } else {
            this.dispatchErrorMessage("Invalid Credentials");
        }
    }

    @Intercepts(RegisterEvent.class)
    @SuppressWarnings("unused")
    public void attemptRegister(RegisterEvent register) {
        String pattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$";
        Optional<UserAccount> maybeUser = this.users.findFirst(u -> u.getUsername().equals(register.username));
        if (maybeUser.isPresent()) {
            this.app.getDispatcher().dispatch(new PopupMessageEvent(PopupMessageEvent.ERROR, "User Already Exists"));
        } else {
            if (register.password.equals(register.passwordMatch) && (register.password.matches(pattern))) {
                final UserAccount user = new UserAccount(register.username, register.password);
                try {
                    this.users.insert(user);
                    this.dispatchRedirect(new LoginView(this.app.getDispatcher()));

                } catch (IOException ex) {
                    this.dispatchErrorMessage("Application error occurred while processing your registration. Please try again later.");
                }

            } else {
                this.dispatchErrorMessage("Password must be min 8 characters alphanumeric, with at least one uppercase character");
            }
        }
    }

}
