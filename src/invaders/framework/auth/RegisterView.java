package invaders.framework.auth;

import invaders.framework.event.Dispatcher;
import invaders.framework.swingui.SwingView;

import javax.swing.*;

class RegisterView extends SwingView {

    private final JLabel usernameLabel;
    private final JLabel passwordLabel;
    private final JLabel passwordMatchLabel;
    private final JTextField usernameInput;
    private final JPasswordField passwordInput;
    private final JPasswordField passwordMatchInput;
    private final JButton registerButton;

    RegisterView(Dispatcher d) {
        super(d, "Register");

        this.getContent().setLayout(null);

        usernameLabel = new JLabel("Please enter new Username");
        passwordLabel = new JLabel("Please enter Password");
        passwordMatchLabel = new JLabel("Please enter password again");
        usernameInput = new JTextField();
        passwordInput = new JPasswordField();
        passwordMatchInput = new JPasswordField();
        registerButton = new JButton("Register");

        usernameLabel.setBounds(80, 70, 200, 30);
        passwordLabel.setBounds(80, 110, 200, 30);
        passwordMatchLabel.setBounds(80, 150, 200, 30);
        usernameInput.setBounds(300, 70, 200, 30);
        passwordInput.setBounds(300, 110, 200, 30);
        passwordMatchInput.setBounds(300, 150, 200, 30);
        registerButton.setBounds(150, 200, 100, 50);

        this.getContent().add(usernameLabel);
        this.addComponent(passwordLabel);
        this.addComponent(passwordMatchLabel);
        this.addComponent(usernameInput);
        this.addComponent(passwordInput);
        this.addComponent(passwordMatchInput);
        this.addComponent(registerButton);

        this.registerButton.addActionListener(e -> getDispatcher().dispatch(new RegisterEvent(getUsernameInput(), getPasswordInput(), getPasswordMatchInput())));
    }

    private String getUsernameInput() {
        return this.usernameInput.getText();
    }

    private String getPasswordInput() {
        return new String(this.passwordInput.getPassword());
    }

    private String getPasswordMatchInput() {
        return new String(this.passwordMatchInput.getPassword());
    }
}
