package invaders.framework;

import invaders.framework.auth.AuthController;
import invaders.framework.auth.LoginView;
import invaders.framework.data.JsonRepository;
import invaders.framework.data.Repository;
import invaders.framework.event.Dispatcher;
import invaders.framework.games.GameApplication;
import invaders.framework.swingui.MainWindow;
import invaders.framework.user.UserAccount;
import invaders.framework.user.UserController;
import invaders.game.SpaceInvadersGame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Framework {
    private static final int WINDOW_WIDTH = 1280;
    private static final int WINDOW_HEIGHT = 720;

    private final List<GameApplication> loadedGames;

    private final Repository<UserAccount> users;
    // Add further Repositories here

    private final UserController userController;
    private final AuthController authController;

    // Add further Controllers here
    private final Dispatcher eventDispatcher;
    private UserAccount loggedInUser;

    private void loadGameApplication(final GameApplication game) {
        this.loadedGames.add(game);
    }

    private Framework(Dispatcher dispatcher) throws IOException {
        this.eventDispatcher = dispatcher;

        this.users = new JsonRepository<>(UserAccount.class, "users.json");
        this.userController = new UserController(this);
        this.authController = new AuthController(this, this.users);
        // Add further Controllers here

        final MainWindow mainWindow = new MainWindow("Invaders", new LoginView(this.eventDispatcher), dispatcher);
        this.eventDispatcher.register(mainWindow);
        this.eventDispatcher.register(this.authController);
        this.eventDispatcher.register(this.userController);

        this.loadedGames = new ArrayList<>();
    }

    public Dispatcher getDispatcher() {
        return this.eventDispatcher;
    }

    public UserAccount getLoggedInUser() {
        return this.loggedInUser;
    }

    public void setLoggedInUser(final UserAccount user) throws IOException {
        this.loggedInUser = user;

        // In the interests of time, we will say that all users own all games.
        this.loggedInUser.setOwnedGames(Arrays.asList(new GameApplication[]{ new SpaceInvadersGame(this.eventDispatcher, user) }));
    }

    public Repository<UserAccount> getUsers() {
        return this.users;
    }

    public static void main(String[] args) {
        final Dispatcher d = new Dispatcher();
        final Framework app;
        try {
            app = new Framework(d);
        } catch (IOException ex) {
            Logger.getLogger(Framework.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
