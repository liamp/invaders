package invaders.framework;

import invaders.framework.swingui.PopupMessageEvent;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;

public abstract class Controller {

    protected final Framework app;

    protected Controller(Framework app) {
        this.app = app;
    }

    protected void dispatchErrorMessage(String errorMessage) {
        this.app.getDispatcher().dispatch(new PopupMessageEvent(PopupMessageEvent.ERROR, errorMessage));
    }

    protected void dispatchInfoMessage(String infoMessage) {
        this.app.getDispatcher().dispatch(new PopupMessageEvent(PopupMessageEvent.INFORMATION, infoMessage));
    }

    protected void dispatchRedirect(final SwingView v) {
        this.app.getDispatcher().dispatch(new Redirect(v));
    }

}
