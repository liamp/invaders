package invaders.framework.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Handles dispatching events to a list of classes that promise they can
 * handle them.
 * <p>
 * Internally, the list of responders is maintained as a map from the event type
 * to an (Object, Method) tuple. This layout makes insertions more expensive but
 * iterations faster, as we can do a single lookup for the type of event we're
 * currently handling and just iterate through the responders.
 *
 * @author Liam
 */
public class Dispatcher {

    /**
     * Maps a given event type to a list of {@link Responder} instances which are invoked once a client calls
     * {@code dispatch} with a context object instance.
     */
    private final Map<Class<?>, List<Responder>> responderIndex;

    public Dispatcher() {
        this.responderIndex = new ConcurrentHashMap<>();
    }

    /**
     * Registers a class with this dispatcher. All of the supplied object's methods which are tagged with the {@link Intercepts}
     * annotation will be added to this dispatcher's list of responders. The classes methods may also be tagged with the
     * optional {@link  Priority} annotation which indicates the order in which tagged methods should be called. The
     * default priority is {@code Priority.Level.NORMAL}
     *
     * @param responderObject An class instance whose methods will be indexed by this dispatcher
     */
    public void register(Object responderObject) {
        Class<?> c = responderObject.getClass();
        while (c != Object.class) {
            for (Method m : c.getDeclaredMethods()) {
                if (m.isAnnotationPresent(Intercepts.class)) {
                    final Class<?> eventType = m.getAnnotation(Intercepts.class).value();
                    m.setAccessible(true);

                    final Priority.Level priority = m.isAnnotationPresent(Priority.class) ? m.getAnnotation(Priority.class).value() : Priority.Level.NORMAL;
                    final Responder responder = new Responder(m, responderObject, priority);
                    final List<Responder> responders;

                    if (! this.responderIndex.containsKey(eventType)) {
                        responders = new ArrayList<>();
                        this.responderIndex.put(eventType, responders);
                    } else {
                        responders = this.responderIndex.get(eventType);
                    }

                    responders.add(responder);

                    // Responders need to be re-sorted on insertion. There is definitely opportunity for optimisation
                    // here; an earlier draft of the code saw a TreeSet in use here, however TreeSets only allow one
                    // Responder per event type, not what we want. Perhaps some kind of Heap? Undoubtedly there exists
                    // some data structure which allows duplicate elements and maintains them in automatic sorted order.
                    Collections.sort(responders);
                }
            }

            c = c.getSuperclass();
        }
    }

    public void unregister(final Object responder) {
        this.responderIndex.values().forEach(eventResponders -> eventResponders.removeIf(q -> q.getResponderObject().equals(responder)));
    }

    /**
     * Emits some data for this dispatcher's registered responders to process.
     *
     * @param event Data object to be passed to each responder in priority order.
     */
    public void dispatch(final Object event) {
        if (this.responderIndex.containsKey(event.getClass())) {
            final List<Responder> eventResponders = this.responderIndex.get(event.getClass());
            eventResponders.forEach(r -> {
                try {
                    r.invoke(event);
                } catch (InvocationTargetException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            });
        }
    }

}