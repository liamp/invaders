package invaders.framework.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@SuppressWarnings("unused")
public @interface Priority {
    enum Level {
        HIGH,
        NORMAL,
        LOW,
    }

    Level value();
}
