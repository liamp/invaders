package invaders.framework.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Utility class which represents a class instance and a particular method of that object which should handle events of
 * a particular type.
 */
class Responder implements Comparable<Responder> {
    private final Method responderMethod;
    private final Object responderObject;
    private final Priority.Level priority;

    Responder(Method responderMethod, Object responderObject, Priority.Level priority) {
        this.responderMethod = responderMethod;
        this.responderObject = responderObject;
        this.priority = priority;
    }

    final Method getResponderMethod() {
        return this.responderMethod;
    }

    final Object getResponderObject() {
        return this.responderObject;
    }

    final Priority.Level getPriority() {
        return this.priority;
    }

    /**
     * Invokes the stored method for this responder with the supplied event object.
     *
     * @param param Event data to pass
     */
    final void invoke(final Object param) throws InvocationTargetException, IllegalAccessException {
        this.responderMethod.invoke(this.responderObject, param);
    }

    @Override
    public int compareTo(Responder o) {
        return this.priority.compareTo(o.priority);
    }
}
