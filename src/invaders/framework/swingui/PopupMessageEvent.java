package invaders.framework.swingui;

public final class PopupMessageEvent {
    public static final byte ERROR = 0x1;
    public static final byte WARNING = 0x2;
    public static final byte INFORMATION = 0x4;

    final byte type;
    final String message;

    public PopupMessageEvent(final byte type, final String message) {
        this.type = type;
        this.message = message;
    }
}