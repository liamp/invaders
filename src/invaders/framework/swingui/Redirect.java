package invaders.framework.swingui;

public final class Redirect {

    // TODO Change me to use the abstract {@link View} class!
    public final SwingView view;

    public Redirect(final SwingView newView) {
        this.view = newView;
    }

    public static final class Back {
        int data = 10;
    }
}
