package invaders.framework.swingui;

import invaders.framework.event.Dispatcher;

import javax.swing.*;
import java.awt.*;

public abstract class SwingView {
    private final JPanel content;
    private final Dispatcher dispatcher;
    private final String title;

    protected SwingView(Dispatcher d, String title) {

        this.dispatcher = d;
        this.title = title;
        this.content = new JPanel();
        this.content.setLayout(null);
    }

    protected final void addComponent(Component comp) {
        this.content.add(comp);
    }

    protected final void addComponent(Component comp, Object constraints) {
        this.content.add(comp, constraints);
    }

    protected Dispatcher getDispatcher() {
        return this.dispatcher;
    }

    protected JPanel getContent() {
        return this.content;
    }

    String getTitle() {
        return this.title;
    }
}
