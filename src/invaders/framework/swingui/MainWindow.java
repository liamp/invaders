package invaders.framework.swingui;

import invaders.framework.event.Dispatcher;
import invaders.framework.event.Intercepts;
import invaders.framework.input.KeyboardInputHandler;

import javax.swing.*;
import java.awt.*;
import java.util.Stack;

public final class MainWindow {
    private final JFrame window;
    private final Dispatcher dispatcher;
    private final Container mainContent;
    private final Stack<SwingView> viewHistory;
    private final JLabel title;
    private SwingView currentView;

    public MainWindow(final String title, final SwingView initialView, final Dispatcher dispatcher) {
        this.window = new JFrame(title);
        this.window.setSize(1280, 720);
        this.window.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        this.window.getContentPane().setLayout(new BorderLayout());

        final JButton backButton = new JButton("Go Back");
        backButton.addActionListener(e -> this.goBack(new Redirect.Back()));

        this.title = new JLabel(initialView.getTitle());

        final JToolBar headerBar = this.createToolBar(backButton, this.title);
        this.window.getContentPane().add(headerBar, BorderLayout.PAGE_START);

        this.mainContent = new JPanel();
        this.mainContent.setLayout(new BorderLayout());
        this.mainContent.add(initialView.getContent(), BorderLayout.CENTER);

        this.window.getContentPane().add(this.mainContent, BorderLayout.CENTER);
        this.window.setVisible(true);

        this.dispatcher = dispatcher;
        this.currentView = initialView;

        this.viewHistory = new Stack<>();
    }

    private JToolBar createToolBar(JButton back, JLabel title) {
        final JToolBar toolbar = new JToolBar();
        toolbar.setLayout(new BorderLayout());
        toolbar.add(back, BorderLayout.WEST);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        toolbar.add(title, BorderLayout.CENTER);
        toolbar.setFloatable(false);

        return toolbar;
    }

    @Intercepts(Redirect.class)
    @SuppressWarnings("unused")
    public void setContent(Redirect next) {
        this.viewHistory.push(this.currentView);
        this.setView(next.view);
        this.currentView.getContent().requestFocus();
        this.currentView.getContent().addKeyListener(new KeyboardInputHandler(this.dispatcher));
    }

    @Intercepts(PopupMessageEvent.class)
    @SuppressWarnings("unused")
    public final void showMessageDialog(PopupMessageEvent msg) {
        int icon = JOptionPane.INFORMATION_MESSAGE;
        switch (msg.type) {
            case PopupMessageEvent.ERROR:
                icon = JOptionPane.ERROR_MESSAGE;
                break;
            case PopupMessageEvent.WARNING:
                icon = JOptionPane.WARNING_MESSAGE;
                break;
        }

        JOptionPane.showMessageDialog(null, msg.message, "", icon);
    }

    @Intercepts(Redirect.Back.class)
    private void goBack(Redirect.Back event) {
        if (!this.viewHistory.isEmpty()) {
            final SwingView lastView = this.viewHistory.pop();
            this.setView(lastView);
        }
    }

    private void setView(final SwingView view) {
        this.dispatcher.unregister(this.currentView);
        this.mainContent.removeAll();
        this.mainContent.add(view.getContent(), BorderLayout.CENTER);
        this.currentView = view;
        this.dispatcher.register(this.currentView);
        this.window.revalidate();
        this.title.setText(view.getTitle());
        this.window.repaint();
    }
}
