package invaders.framework.user;

import invaders.framework.data.Model;
import invaders.framework.games.GameApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class UserAccount extends Model {
    private static final AtomicInteger AUTO_INCREMENTER = new AtomicInteger();
    private final ArrayList<UserAccount> friendList;
    private String username;
    private String password;
    private double coins;
    private int hiScore;
    private int gamesPlayed;

    private transient List<GameApplication> ownedGames;

    public UserAccount(String username, String password) {
        this.username = username;
        this.password = password;
        this.coins = 0.0;
        this.hiScore = 0;
        this.gamesPlayed = 0;
        this.friendList = new ArrayList<>();
    }

    public void setOwnedGames(final List<GameApplication> games) {
        this.ownedGames = games;
    }

    // Possibly add a guest functionality so we would need another constructor in which there would be an existing
    // coins, hiScore and gamesPlayed

    public void setPassword(String password) {
        this.password = password;
    }

    public int incrementGamesPlayed() {
        return ++this.gamesPlayed;
    }

    public void changeCoinBalance(double coinAmount) {
        if (coinAmount > 0.0) {
            this.coins += coinAmount;
        } else if (coinAmount < 0.0) {
            this.coins -= coinAmount;
        }
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getCoins() {
        return coins;
    }

    public int getHiScore() {
        return hiScore;
    }

    public void setHiScore(int hiScore) {
        this.hiScore = hiScore;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public UserAccount[] getFriendList() {
        return friendList.toArray(new UserAccount[0]);
    }

    public boolean validateCredentials(String username, String password) {
        return this.username.equals(username) && this.password.equals(password);
    }

    public void addFriend(UserAccount newFriend) {
        this.friendList.add(newFriend);
    }

    public List<GameApplication> getOwnedGames() {
        return this.ownedGames;
    }
}

