package invaders.framework.user;

import invaders.framework.event.Dispatcher;
import invaders.framework.games.GameApplication;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

public final class DashboardView extends SwingView {

    public DashboardView(Dispatcher d, UserAccount user) {
        super(d, "Dashboard");

        super.getContent().setLayout(new BorderLayout());

        final JPanel headerContainer = new JPanel();
        headerContainer.setLayout(new BorderLayout());

        final JLabel helloLabel = new JLabel("Hello " + user.getUsername());
        helloLabel.setHorizontalAlignment(JLabel.CENTER);
        helloLabel.setBorder(new EmptyBorder(10, 0, 10, 0));
        headerContainer.add(helloLabel, BorderLayout.CENTER);

        final JButton myAccountButton = new JButton("My Account");
        myAccountButton.addActionListener(e -> d.dispatch(new Redirect(new AccountView(d, user))));

        final JButton myGamesButton = new JButton("My Games");
        myGamesButton.addActionListener(e -> d.dispatch(new Redirect(new GamesListView(d, user.getOwnedGames()))));

        headerContainer.add(myGamesButton, BorderLayout.WEST);
        headerContainer.add(myAccountButton, BorderLayout.EAST);

        this.addComponent(headerContainer, BorderLayout.PAGE_START);
    }
}
