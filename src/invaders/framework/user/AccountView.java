package invaders.framework.user;

import invaders.framework.event.Dispatcher;
import invaders.framework.swingui.SwingView;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

final class AccountView extends SwingView {

    AccountView(Dispatcher d, UserAccount user) {
        super(d, "My Account");
        this.getContent().setLayout(new BorderLayout());
        final JPanel textContainer = new JPanel();
        textContainer.setLayout(new BoxLayout(textContainer, BoxLayout.Y_AXIS));

        final JLabel accountLabel = new JLabel(user.getUsername() + "'s Account");
        final JLabel hiScoreLabel = new JLabel("Hi-Score: " + user.getHiScore());
        final JLabel gamesPlayedLabel = new JLabel("Games Played: " + user.getHiScore());
        accountLabel.setHorizontalAlignment(JLabel.CENTER);

        textContainer.setBorder(new EmptyBorder(50, 10, 10, 50));
        accountLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        hiScoreLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        gamesPlayedLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        textContainer.add(accountLabel);
        textContainer.add(hiScoreLabel);
        textContainer.add(gamesPlayedLabel);
        this.addComponent(textContainer, BorderLayout.CENTER);
    }
}
