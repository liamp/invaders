package invaders.framework.user;

import invaders.framework.Controller;
import invaders.framework.Framework;
import invaders.framework.data.Repository;

// Add Responds

public class UserController extends Controller {

    private final Repository<UserAccount> users;

    public UserController(Framework app) {
        super(app);
        this.users = app.getUsers();
    }

    // Add functionality as needed
}

//repository = "users"
