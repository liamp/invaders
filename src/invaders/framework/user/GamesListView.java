package invaders.framework.user;

import invaders.framework.event.Dispatcher;
import invaders.framework.games.GameApplication;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.util.List;

public final class GamesListView extends SwingView {

    private final JList<GameApplication> gamesList;

    public GamesListView(Dispatcher d, List<GameApplication> games) {
        super(d, "My Games");
        this.getContent().setLayout(new BorderLayout());
        this.gamesList = new JList<>(games.toArray(new GameApplication[0]));
        this.gamesList.setCellRenderer(new GamesListAdapter());
        this.gamesList.setVisible(true);
        this.getContent().add(gamesList, BorderLayout.CENTER);

        this.gamesList.addListSelectionListener(this::onListItemClick);
    }

    private void onListItemClick(ListSelectionEvent event) {
        if (!event.getValueIsAdjusting()) {
            final GameApplication game = this.gamesList.getSelectedValue();
            this.getDispatcher().dispatch(new Redirect(new SaveGameView(this.getDispatcher(), game)));
        }
    }

    private static final class GamesListAdapter extends JLabel implements ListCellRenderer<GameApplication> {
        private GamesListAdapter() {
            this.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends GameApplication> list, GameApplication value, int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                this.setBackground(Color.BLUE);
            }

            this.setText(value.getGameTitle());

            return this;
        }
    }
}
