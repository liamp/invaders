/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package invaders.framework.user;

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.event.Dispatcher;
import invaders.framework.games.GameApplication;
import invaders.framework.games.GameView;
import invaders.framework.swingui.Redirect;
import invaders.framework.swingui.SwingView;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * @author ericn
 */
final class SaveGameView extends SwingView {
    private final JList<EntityDatabase.Snapshot> savedGames;
    private final GameApplication game;

    SaveGameView(Dispatcher d, final GameApplication game) {

        super(d, "Saved games");
        this.getContent().setLayout(new FlowLayout());
        this.savedGames = new JList<>(game.getSaves().toArray(new EntityDatabase.Snapshot[0]));
        this.getContent().add(savedGames);
        final JButton newGameButton = new JButton("New Game");
        this.getContent().add(newGameButton);
        this.game = game;
        this.savedGames.setCellRenderer(new SavedGamesListAdapter());
        newGameButton.setVisible(true);
        this.savedGames.setVisible(true);
        newGameButton.addActionListener(this::onNewGameButtonClick);
        this.savedGames.addListSelectionListener(this::onListItemClick);
    }

    private void onListItemClick(ListSelectionEvent event) {
        if (!event.getValueIsAdjusting()) {
            final EntityDatabase.Snapshot save = this.savedGames.getSelectedValue();
            //Pass in the database snapshot of the save clicked
            EntityDatabase eDB = new EntityDatabase();
            eDB.restore(save);
            this.game.loadGame(save);
            this.getDispatcher().dispatch(new Redirect(new GameView(this.getDispatcher(), game)));

        }
    }

    private void onNewGameButtonClick(ActionEvent event) {
        this.getDispatcher().dispatch(new Redirect(new GameView(this.getDispatcher(), game)));
    }

    private static final class SavedGamesListAdapter extends JLabel implements ListCellRenderer<EntityDatabase.Snapshot> {
        private SavedGamesListAdapter() {
            this.setOpaque(true);
        }

        @Override
        public Component getListCellRendererComponent(JList<? extends EntityDatabase.Snapshot> list, EntityDatabase.Snapshot value, int index, boolean isSelected, boolean cellHasFocus) {
            if (isSelected) {
                this.setBackground(Color.BLUE);
            }
            this.setText(value.getTitle());
            return this;
        }
    }
}
