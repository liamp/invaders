
package invaders.framework.input;

import invaders.framework.event.Dispatcher;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyboardInputHandler implements KeyListener {
    private final Dispatcher dispatcher;

    public KeyboardInputHandler(final Dispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
    }

    @Override
    public void keyPressed(KeyEvent e) { //on keypress it passes the command to Invoker in framework.input
        int keyCode = e.getKeyCode();
        this.dispatcher.dispatch(new KeyInputEvent(keyCode));
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
    }
}
