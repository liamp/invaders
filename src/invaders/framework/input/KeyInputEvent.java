package invaders.framework.input;

public class KeyInputEvent {
    public final int keyCode;


    KeyInputEvent(int keyCode) {
        this.keyCode = keyCode;
    }
}
