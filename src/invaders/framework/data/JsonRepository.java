package invaders.framework.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class JsonRepository<M extends Model> implements Repository<M> {

    private GsonBuilder builder = new GsonBuilder();

    private final Gson gson;

    private final Path filePath;
    private final ArrayList<M> data;
    private final Class<M> modelType;

    // Model type required is an unfortunate hack to get serialisation to work properly due to
    // Java's brain-damaged generics system.
    public JsonRepository(final Class<M> modelType, final String fileName) throws IOException {
        this.filePath = Paths.get(fileName);

        if (Files.notExists(this.filePath)) {
            Files.createFile(this.filePath);
        }

        // Load the entire JSON file into a UTF-8 encoded string.
        final String json = new String(Files.readAllBytes(this.filePath), StandardCharsets.UTF_8);

        final Type modelListType = TypeToken.getParameterized(ArrayList.class, modelType).getType();

        this.gson = this.builder.enableComplexMapKeySerialization().create();

        final ArrayList<M> data = this.gson.fromJson(json, modelListType);
        if (data != null) {
            this.data = this.gson.fromJson(json, modelListType);
        } else {
            this.data = new ArrayList<>();
        }
        this.modelType = modelType;
    }

    public JsonRepository(Class<M> modelType, final String fileName, JsonDeserializer<M> deserializer) throws IOException {
        this.filePath = Paths.get(fileName);

        if (Files.notExists(this.filePath)) {
            Files.createFile(this.filePath);
        }

        // Load the entire JSON file into a UTF-8 encoded string.
        final String json = new String(Files.readAllBytes(this.filePath), StandardCharsets.UTF_8);

        final Type modelListType = TypeToken.getParameterized(ArrayList.class, modelType).getType();

        this.gson = this.builder.enableComplexMapKeySerialization().registerTypeAdapter(modelType, deserializer).create();

        final ArrayList<M> data = this.gson.fromJson(json, modelListType);
        if (data != null) {
            this.data = this.gson.fromJson(json, modelListType);
        } else {
            this.data = new ArrayList<>();
        }
        this.modelType = modelType;
    }

    @Override
    public void save() throws IOException {
        final Type modelListType = TypeToken.getParameterized(ArrayList.class, this.modelType).getType();
        final String json = this.gson.toJson(this.data, modelListType);
        Files.write(this.filePath, json.getBytes());
    }

    @Override
    public Optional<M> findFirst(Predicate<M> query) {
        return this.data.stream().filter(query).findFirst();
    }

    @Override
    public Stream<M> findAll(Predicate<M> query) {
        return this.data.stream().filter(query);
    }

    @Override
    public void update(Consumer<M> transformer) throws IOException {
        this.data.forEach(transformer);
        this.save();
    }

    @Override
    public void insert(M m) throws IOException {
        this.data.add(m);
        this.save();
    }

    @Override
    public void delete(M m) throws IOException {
        this.data.remove(m);
        this.save();
    }

    @Override
    public List<M> all() {
        return Collections.unmodifiableList(this.data);
    }
}
