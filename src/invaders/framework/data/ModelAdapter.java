package invaders.framework.data;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class ModelAdapter<A> extends Model {
    private final A wrapped;

    private ModelAdapter(final A wrapped) {
        super();

        this.wrapped = wrapped;
    }

    public static <Z> ModelAdapter<Z> of(Z data) {
        return new ModelAdapter<>(data);
    }

    @SuppressWarnings("unchecked")
    public static <Z> Class<ModelAdapter<Z>> getWrappedClass(final Class<Z> unwrappedType) {
        final TypeToken z = TypeToken.getParameterized(ModelAdapter.class, unwrappedType);
        final Type t = z.getType();
        final ParameterizedType pt = (ParameterizedType) t;

        return (Class<ModelAdapter<Z>>) pt.getRawType();
    }

    public final A get() {
        return this.wrapped;
    }
}
