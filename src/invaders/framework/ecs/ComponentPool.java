package invaders.framework.ecs;

import java.lang.reflect.Array;
import java.util.function.Supplier;

/**
 * Methods:
 * <p>
 * Allocate an object. If we have reached the end of the pool, recycle
 * C allocate();
 *
 * @param <C>
 */
public abstract class ComponentPool<C extends Component> {

    private final Class<C> componentType;

    private final C[] dataStore;
    private final int capacity;

    private int used;

    @SuppressWarnings("unchecked")
    public ComponentPool(final Class<C> componentType, final Supplier<C> prototype, int capacity) {
        this.componentType = componentType;
        this.capacity = capacity;

        this.dataStore = (C[]) Array.newInstance(componentType, capacity);

        for (int i = 0; i < capacity; ++i) {
            this.dataStore[i] = prototype.get();
        }

        this.used = 0;
    }

    public abstract C reset(C component);

    public synchronized C allocate() {
        final C out;

        if (this.used >= this.capacity) {
            this.used = 0;
            out = this.reset(this.dataStore[this.used]);
        } else {
            out = this.dataStore[this.used];
        }

        ++this.used;

        return out;
    }

}
