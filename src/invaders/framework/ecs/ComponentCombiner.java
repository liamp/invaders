package invaders.framework.ecs;

@FunctionalInterface
public interface ComponentCombiner<A extends Component, B extends Component> {
    void combine(A inout, final B in);
}
