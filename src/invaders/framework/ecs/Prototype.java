package invaders.framework.ecs;

/**
 *
 * @author kyran
 */

public interface Prototype<C extends Component> {

    C reproduce();

    default boolean equals(C c) {
        return false;
    }
}
