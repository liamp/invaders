package invaders.framework.ecs;

import java.util.concurrent.atomic.AtomicInteger;

public final class EntityId {
    private static final AtomicInteger AUTO_INCREMENT = new AtomicInteger();

    private final int id;

    public EntityId() {
        this.id = AUTO_INCREMENT.getAndIncrement();
    }

    public EntityId(int asInt) {
        this.id = asInt;
    }

    @Override
    public boolean equals(final Object other) {
        if (other instanceof EntityId) {
            return ((EntityId) other).id == this.id;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.id;
    }
}
