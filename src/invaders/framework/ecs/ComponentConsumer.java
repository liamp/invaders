package invaders.framework.ecs;

public interface ComponentConsumer<C extends Component> {

    default ComponentConsumer<C> andThen(ComponentConsumer<C> after) {
        return (component, entity) -> {
            this.processComponent(component, entity);
            after.processComponent(component, entity);
        };
    }

    void processComponent(C component, EntityId entity);
}
