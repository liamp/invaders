package invaders.framework.ecs;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import invaders.framework.data.ModelAdapter;

import java.lang.reflect.Type;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class EntityDatabase {

    private Map<String, Map<EntityId, Component>> componentsToEntities;

    public EntityDatabase() {
        this.componentsToEntities = new HashMap<>();
    }

    // Copy constructor
    private EntityDatabase(final EntityDatabase other) {
        this.componentsToEntities = new HashMap<>();

        // Need to copy all the rows as well.
        other.componentsToEntities.forEach((k, v) -> {
            final Map<EntityId, Component> clonedRow = new HashMap<>(v);
            this.componentsToEntities.put(k, clonedRow);
        });
    }

    public <C extends Component> Optional<Map<EntityId, Component>> getComponentTable(Class<C> componentClass) {
        return Optional.ofNullable(this.componentsToEntities.get(componentClass.getCanonicalName()));
    }

    @SuppressWarnings("unchecked")
    public <C extends Component> void applyConsumer(ComponentConsumer<C> p, Class<C> componentClass) {
        final Optional<Map<EntityId, Component>> maybeTable = this.getComponentTable(componentClass);
        maybeTable.ifPresent(table -> table.forEach((k, v) -> p.processComponent((C) v, k)));
    }

    @SuppressWarnings("unchecked")
    public <A extends Component, B extends Component> void applyCombiner(ComponentCombiner<A, B> componentCombiner, Class<A> classA, Class<B> classB) {
        final Optional<Map<EntityId, Component>> entitiesA = this.getComponentTable(classA);
        final Optional<Map<EntityId, Component>> entitiesB = this.getComponentTable(classB);

        if (entitiesA.isPresent() && entitiesB.isPresent()) {
            entitiesA.get().forEach((entityId, component) -> {
                final Component cB = entitiesB.get().get(entityId);
                if (cB != null) {
                    componentCombiner.combine((A) component, (B) cB);
                }
            });
        }
    }


    public <C extends Component> void mapForEntities(Stream<EntityId> ids, Consumer<C> processor, Class<C> cClass) {
        final Optional<Map<EntityId, Component>> entities = this.getComponentTable(cClass);

        entities.ifPresent(entityIdComponentMap -> ids.forEach(id -> processor.accept((C) entityIdComponentMap.get(id))));
    }

    public <A extends Component, B extends Component, F extends Component> void combineWithFilter(ComponentCombiner<A, B> combiner, Class<A> classA, Class<B> classB, Predicate<F> filter, Class<F> filterClass) {
        final Optional<Map<EntityId, Component>> maybeTableA = this.getComponentTable(classA);
        final Optional<Map<EntityId, Component>> maybeTableB = this.getComponentTable(classB);
        final Optional<Map<EntityId, Component>> filterTable = this.getComponentTable(filterClass);

        if (maybeTableA.isPresent() && maybeTableB.isPresent() && filterTable.isPresent()) {
            maybeTableA.get().forEach((entityId, component) -> {
                if (filter.test((F) filterTable.get().get(entityId))) {
                    final Component cB = maybeTableB.get().get(entityId);
                    if (cB != null) {
                        combiner.combine((A) component, (B) cB);
                    }
                }
            });
        }
    }

    @SuppressWarnings("unchecked")
    public <C extends Component> Stream<EntityId> findAll(Predicate<C> query, Class<C> componentClass) {
        final Optional<Map<EntityId, Component>> entities = this.getComponentTable(componentClass);

        return entities.map(entityIdComponentMap -> entityIdComponentMap.entrySet()
                .stream()
                .filter(entry -> query.test((C) entry.getValue()))
                .map(Map.Entry::getKey)).orElseGet(Stream::empty);
    }

    public <C extends Component> Optional<EntityId> findFirst(Predicate<C> query, Class<C> componentClass) {
        final Optional<Map<EntityId, Component>> maybeTable = this.getComponentTable(componentClass);

        return maybeTable.flatMap(table -> table.entrySet()
                .stream()
                .filter(e -> query.test((C) e.getValue()))
                .map(Map.Entry::getKey).findFirst());
    }

    // TODO(Liam): Better name here??
    public <C extends Component> void addComponentToEntity(EntityId entity, C c) {
        final String componentClass = c.getClass().getCanonicalName();
        Objects.requireNonNull(entity);

        if (this.componentsToEntities.containsKey(componentClass)) {
            this.componentsToEntities.get(componentClass).put(entity, c);
        } else {
            Map<EntityId, Component> entityToComponent = new HashMap<>(8);
            entityToComponent.put(entity, c);
            this.componentsToEntities.put(componentClass, entityToComponent);
        }
    }

    public void removeComponentFromEntity(EntityId entity, Class<? extends Component> componentClass) {
        final Map<EntityId, Component> entitiesForComponent = this.componentsToEntities.get(componentClass.getCanonicalName());

        if (entitiesForComponent != null) {
            entitiesForComponent.remove(entity);
        }
    }

    public Snapshot saveState() {
        return new Snapshot(this);
    }

    public void restore(Snapshot savedState) {
        this.componentsToEntities = savedState.getSavedState().componentsToEntities;
    }

    public Map<EntityId, Component> getEntitiesForComponent(final Class<? extends Component> componentType) {
        return new HashMap<>(this.componentsToEntities.get(componentType.getCanonicalName()));
    }

    public void deleteEntity(final EntityId entity) {
        this.componentsToEntities.values().forEach(table -> table.remove(entity));
    }

    public void deleteAll(final Set<EntityId> toDelete) {
        this.componentsToEntities.values().forEach(componentTable -> componentTable.keySet().removeAll(toDelete));
    }

    public static class InterfaceAdapter implements JsonDeserializer<ModelAdapter<EntityDatabase.Snapshot>> {
        @Override
        public ModelAdapter<EntityDatabase.Snapshot> deserialize(JsonElement jsonElement, Type type,
                             JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            final EntityDatabase d = new EntityDatabase();
            final JsonElement componentsToEntities = jsonElement.getAsJsonObject().getAsJsonObject("wrapped").getAsJsonObject("savedState").getAsJsonObject("componentsToEntities");
            final Map<String, Map<EntityId, Component>> index = new HashMap<>();
            componentsToEntities.getAsJsonObject().entrySet().forEach(entry -> {
                final Map<EntityId, Component> componentTable;
                try {
                    final Class<?> componentClass = Class.forName(entry.getKey());
                    assert(componentClass.isAssignableFrom(Component.class)); // Secure? You tell me

                    final Type mapType = TypeToken.getParameterized(HashMap.class, EntityId.class, componentClass).getType();
                    //final EntityId id = new EntityId(entry.getValue().getAsJsonArray().getAsJsonArray().get(0).getAsInt());
                    componentTable = jsonDeserializationContext.deserialize(entry.getValue(), mapType);
                    index.put(entry.getKey(), componentTable);
                    //final Object c = componentsToEntities.getAsJsonArray().getAsJsonArray().get(1).getAsJsonObject();


                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }

            });

            d.componentsToEntities = index;
            return ModelAdapter.of(d.saveState());
        }
    }

    public static final class Snapshot {
        private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy/MM/dd");

        private final EntityDatabase savedState;
        private final String saveGameTitle;

        private Snapshot(EntityDatabase instance) {
            this.savedState = new EntityDatabase(instance);
            this.saveGameTitle = TIME_FORMATTER.format(LocalDate.now());
        }

        private EntityDatabase getSavedState() {
            return this.savedState;
        }
        public String getTitle(){
            return saveGameTitle;
        }
    }
}
