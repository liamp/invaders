/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import invaders.framework.event.Dispatcher;
import invaders.framework.event.Intercepts;
import invaders.framework.event.Priority;
import invaders.framework.event.Priority.Level;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author ericn
 */
public class DispatcherTest {
    private static class TestEvent {
    }

    private boolean responded = false;
    private Dispatcher d = new Dispatcher();

    private final ArrayList<Level> calledOrder = new ArrayList<>();

    public DispatcherTest() {
        this.d.register(this);
    }

    @Intercepts(TestEvent.class)
    public void normalResponder(TestEvent tE) {
        this.calledOrder.add(Level.NORMAL);
        responded = true;
    }

    @Test
    public void testDispatcherMethodCalled() {
        this.d.dispatch(new TestEvent());
        assertTrue(responded);
    }

    @Intercepts(TestEvent.class)
    @Priority(Level.HIGH)
    public void highResponder(final TestEvent e) {
        this.calledOrder.add(Level.HIGH);
    }

    @Intercepts(TestEvent.class)
    @Priority(Level.LOW)
    public void lowResponder(final TestEvent e) {
        this.calledOrder.add(Level.LOW);
    }

    @Test
    public void testDispatcherMethodCalledInOrder() {
        this.d.dispatch(new TestEvent());

        final List<Priority.Level> expected = Arrays.asList(Level.HIGH, Level.NORMAL, Level.LOW);

        assertEquals(expected, this.calledOrder);
    }
}
