/*
 * Given I have an entity creator
 * When I create a Entity and add it to the componant
 * Then the entity would be created#
 * When I remove the entity form the componant
 * Then the entity will be removed
 */

import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.game.entity.HealthComponent;
import org.junit.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * @author ericn
 */
public class EntityCreationTest {
    private final EntityDatabase eDB;
    private final EntityId testEntity;
    private final HealthComponent testHealth;

    public EntityCreationTest() {
        eDB = new EntityDatabase();
        testEntity = new EntityId();
        testHealth = new HealthComponent(100);
    }
    @Test
    public void entityCreator() {
        eDB.addComponentToEntity(testEntity, testHealth);
        assertTrue(eDB.getEntitiesForComponent(HealthComponent.class).containsKey(testEntity));
        eDB.removeComponentFromEntity(testEntity, HealthComponent.class);
        assertFalse(eDB.getEntitiesForComponent(HealthComponent.class).containsKey(testEntity));
    }
}
