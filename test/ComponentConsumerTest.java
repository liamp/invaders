/*
 Step 1 create the entites
 Step 2 Process them
 Step 3 put all entites in a table
 Step 4 Process entites
 Step 5 Put processed entites in a different table
 Step 6 compare both
 
*/

import invaders.framework.ecs.ComponentConsumer;
import invaders.framework.ecs.EntityDatabase;
import invaders.framework.ecs.EntityId;
import invaders.game.entity.HealthComponent;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * @author ericn
 */
public class ComponentConsumerTest {
    private final EntityDatabase eDB;
    private final EntityId eID;
    private final HealthComponent health;
    private final ComponentConsumer<HealthComponent> eP;
    private List<EntityId> beforeList;
    private List<EntityId> afterList;


    public ComponentConsumerTest() {
        this.eDB = new EntityDatabase();
        this.eID = new EntityId();
        this.health = new HealthComponent(100);
        this.eP = this::process;
        this.beforeList = new ArrayList<>();
        this.afterList = new ArrayList<>();
    }

    private void createBeforeList() {
        for (int i = 0; i < 5; i++) {
            EntityId eID = new EntityId();
            this.eDB.addComponentToEntity(eID, health);
            beforeList.add(eID);
        }
    }

    private void process(HealthComponent health, EntityId eID) {
        afterList.add(eID);
    }

    @Test
    public void entityProcessTest() {
        boolean isEqual;
        createBeforeList();
        eDB.applyConsumer(eP, HealthComponent.class);

        isEqual = afterList.containsAll(beforeList);
        assertTrue(isEqual);
    }

}
