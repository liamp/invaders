/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import invaders.framework.event.Dispatcher;
import invaders.framework.event.Intercepts;
import invaders.game.input.ActionCommand;
import org.junit.Test;

import static org.junit.Assert.assertTrue;


/**
 * @author Eirc Nolan
 */
public class CommandPatternTest {
    private static class CommandEvent { }

    private Dispatcher d = new Dispatcher();
    private boolean commandPressed = false;

    public CommandPatternTest() {
        this.d.register(this);
    }

    @Intercepts(CommandEvent.class)
    public void commandResponder(CommandEvent cE) {
        CommandTest test = new CommandTest();
        test.execute();
    }

    @Test
    public void commandPatternTest() {
        this.d.dispatch(new CommandEvent());
        assertTrue(commandPressed);
    }

    class CommandTest implements ActionCommand {
        @Override
        public void execute() {
            commandPressed = !commandPressed;
        }
    }
}
