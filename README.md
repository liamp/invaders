# Space Invaders
This is a simple Java Space Invaders clone developed for the University of 
Limerick CS4227 Software Architecture group project.

## Authors
* Eric Nolan
* Kyran Healy
* Liam Power
* Daniel Keighley 
